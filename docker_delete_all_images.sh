#!/bin/bash

set -e

echo "WARNING"
echo "All docker files will be removed!"
echo "Press enter to continue, or ctrl+c"
read

echo "[*] Removing images"
docker stop $(docker ps -a -q) && docker rm $(docker ps -a -q)
