#!/bin/bash

# Colors
# Only run `tput` if session is interactive and TTY is assigned
if test -t 1; then
    RED=$(tput setaf 1)
    YELLOW=$(tput setaf 3)
    CYAN=$(tput setaf 6)
    NORMAL=$(tput sgr0)
else
    RED=''
    YELLOW=''
    CYAN=''
    NORMAL=''
fi


# spin for x seconds
_spinner()
{
    local -a frames=( '/' '-' '\' '|' )
    for x in `seq 1 $1`; do 
        echo -ne "${frames[i++ % ${#frames[@]}]}"
        sleep 1
        echo -ne "\b"
    done
}

_error()
{
    printf "${RED}[*] Error: ${NORMAL}$1"
    echo ""
}

_warning()
{
    printf "${YELLOW}[*] Warning: ${NORMAL}$1"
    echo ""
}

_info()
{
    printf "${CYAN}[*] Info: ${NORMAL}$1"
    echo ""
}

# Print a horizontal rule
# Only if TTY is set
_rule ()
{
    if test -t 1; then
        printf -v _hr "%*s" $(tput cols) && echo ${_hr// /${1--}}
    fi
}


command_exists()
{
    command -v "$@" > /dev/null 2>&1
}
