#!/bin/bash

# Print a horizontal rule
# Only if TTY is set
_rule ()
{
    if test -t 1; then
        printf -v _hr "%*s" $(tput cols) && echo ${_hr// /${1--}}
    fi
}

check_runners()
{
    echo "This script will help you resolve problem like:"
    echo "\"ERROR: Build failed: API error (500): service endpoint with name runner-ccecac93-project-352-concurrent-0-build already exists\""
    echo "This issue is described here: https://gitlab.com/gitlab-org/gitlab-ci-multi-runner/issues/1642"
    echo ""
    echo ""
    echo "To continue press enter"
    read

    _rule
    echo "Running build: "
    docker ps | grep "runner-"
    echo ""
    echo ""
    echo "To continue press enter"
    read

    _rule
    echo "Network usages: "
    docker network inspect bridge | grep "runner-"
    echo ""
    echo ""
    echo "To continue press enter"
    read

    _rule
    echo "If you see some network usages that are not runned this is probably an issue. You should remove this network connection"
    echo "To remove that image you need to execute:"
    echo "$ docker network disconnect -f bridge runner-d12179aa-project-352-concurrent-0-build"
    echo "For more informations visit: https://github.com/docker/docker/issues/20398"
    _rule
    echo ""
    echo ""

    echo "To continue press enter"
    read
}

check_runners
