#!/bin/bash

set -e

echo "WARNING"
echo "All docker files that are currenlty not runned will be removed!"
echo "Press enter to continue, or ctrl+c"
read

echo "[*] Removing stopped images"
docker ps -a -q -f status=exited | xargs --no-run-if-empty docker rm || echo "[*] Something went wrong"

echo "[*] Removing not used images"
docker images -q | xargs --no-run-if-empty docker rmi || echo "[*] Something went wrong"

echo "[*] Removing orphaned volumes"
docker volume ls -qf dangling=true | xargs -r docker volume rm || echo "[*] Something went wrong"
